package br.com.dog.trainer.model;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Aula {
	@Id @GeneratedValue
	private Long id;
	
	private String nome;
	private String descricao;

	@OneToOne
	private Adestrador adestrador;
	
	@OneToMany
	private Collection<Comando> comandos;
	
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	

	public Adestrador getAdestrador() {
		return adestrador;
	}

	public void setAdestrador(Adestrador adestrador) {
		this.adestrador = adestrador;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Collection<Comando> getComandos() {
		return comandos;
	}

	public void setComandos(Collection<Comando> comandos) {
		this.comandos = comandos;
	}
}
