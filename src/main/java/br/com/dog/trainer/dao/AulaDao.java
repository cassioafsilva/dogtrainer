package br.com.dog.trainer.dao;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.dog.trainer.model.Aula;

public class AulaDao extends GenericDao<Long, Aula> {

	
	@Inject
	public AulaDao(EntityManager entityManager) {
		
		super(entityManager);
	}
}
