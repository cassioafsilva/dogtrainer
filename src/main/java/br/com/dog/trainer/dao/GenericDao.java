package br.com.dog.trainer.dao;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

@SuppressWarnings("unchecked")
public class GenericDao<PK, T> {
    
	@Inject
	protected EntityManager entityManager;

	public GenericDao() {
		this(null);
	}
	
    @Inject
    public GenericDao(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
    

	public T buscarPorId(PK pk) {
        return (T) entityManager.find(getTypeClass(), pk);
    }
	
	
	public void inserir(T entity){
		entityManager.persist(entity);
	}
	
	public void alterar(T entity) {
        entityManager.merge(entity);
    }
 
    public void excluir(T entity) {
    	Object c=entityManager.merge(entity); 
    	
        entityManager.remove(c);
    }
	
    
    
    public List<T> buscarTodos() {
        return entityManager.createQuery(("FROM " + getTypeClass().getName()))
                .getResultList();
    }
    
    private Class<?> getTypeClass() {
        Class<?> clazz = (Class<?>) ((ParameterizedType) this.getClass()
                .getGenericSuperclass()).getActualTypeArguments()[1];
        return clazz;
    }
    
	public List<T> buscarTodosDoAdestrador(Long idAdestrador) {
	 	Query query = entityManager.createQuery(" from "+ this.getTypeClass().getName() +" where adestrador_id = ?" , getTypeClass());
		query.setParameter(1, idAdestrador);
		return query.getResultList();
	}
    
}
