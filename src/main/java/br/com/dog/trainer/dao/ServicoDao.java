package br.com.dog.trainer.dao;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.dog.trainer.model.Servico;


public class ServicoDao extends GenericDao<Long, Servico>{
	
	@Inject
	public ServicoDao(EntityManager entityManager) {
		
		super(entityManager);
	}
}

