package br.com.dog.trainer.dao;

 import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import br.com.dog.trainer.model.Proprietario;

@RequestScoped
public class ProprietarioDao extends GenericDao<Long, Proprietario>{


	@Inject
	public ProprietarioDao() {
		super();
	}

}
