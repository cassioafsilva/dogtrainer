package br.com.dog.trainer.dao;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.dog.trainer.model.Comando;

public class ComandoDao extends GenericDao<Long, Comando>{
	
	@Inject
	public ComandoDao(EntityManager entityManager) {
		
		super(entityManager);
	}
}
