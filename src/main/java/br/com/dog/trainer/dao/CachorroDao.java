package br.com.dog.trainer.dao;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.dog.trainer.model.Cachorro;

public class CachorroDao extends GenericDao<Long, Cachorro>{

	@Inject
	public CachorroDao(EntityManager entityManager) {
		
		super(entityManager);
	}
}
