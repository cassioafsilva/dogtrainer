package br.com.dog.trainer.dao;

 import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import br.com.dog.trainer.model.Adestrador;

@RequestScoped
public class AdestradorDao extends GenericDao<Long, Adestrador>{


	@Inject
	public AdestradorDao() {
		super();
	}
}
