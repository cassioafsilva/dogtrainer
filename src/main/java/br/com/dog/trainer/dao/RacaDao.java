package br.com.dog.trainer.dao;

import javax.inject.Inject;

import br.com.dog.trainer.model.Raca;

public class RacaDao extends GenericDao<Long, Raca> {

	@Inject
	public RacaDao() {
		super();
	}
}
