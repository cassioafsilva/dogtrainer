package br.com.dog.trainer.controller;

import javax.inject.Inject;

import br.com.caelum.vraptor.Result;
import br.com.dog.trainer.model.Adestrador;
import br.com.dog.trainer.sessao.UsuarioLogado;

public class BaseController {

	@Inject protected UsuarioLogado usuarioLogado;
	@Inject protected Adestrador adestrador;
	@Inject protected Result result;
	
	
	public Long getIdAdestrador() {
		return usuarioLogado.getUtilizadorDoSistema().getId();
	}
	
	
}
