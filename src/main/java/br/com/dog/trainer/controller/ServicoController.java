package br.com.dog.trainer.controller;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.dog.trainer.dao.ServicoDao;
import br.com.dog.trainer.model.Servico;
import br.com.dog.trainer.sessao.UsuarioLogado;

@Controller
public class ServicoController extends BaseController{

	@Inject private ServicoDao servicoDao;
	@Inject UsuarioLogado usuarioLogado;

	
	@Path("/formServico")
	public void formServico() {
		getIdAdestrador();
	}

	@Path("/listarServicos")
	public void listarServicos() {
		result.include("servicos",servicoDao.buscarTodosDoAdestrador(getIdAdestrador()));
	}
	
	@Get("/servico/verServico/{servico.id}")
	public Servico verServico(Servico servico) {
		return servicoDao.buscarPorId(servico.getId());
	}

	@Post("/servico")
	public void insert(Servico servico) {
		
		this.setAdestrador(servico);
		
		servicoDao.inserir(servico);
		result.redirectTo(this).verServico(servico);
	}
	
	@Get("/servico/editarServico/{servico.id}")
	public Servico editarServico(Servico servico){
		return servicoDao.buscarPorId(servico.getId());
	}
	
	@Put("/servico")
	public void update(Servico servico) {
		this.setAdestrador(servico);
		servicoDao.alterar(servico);
		result.redirectTo(this).verServico(servico);
	}
	
	@Delete("/servico/{servico.id}")
	public void delete(Servico servico) {
		servicoDao.excluir(servico);
		result.redirectTo(this).listarServicos();
	}


	private void setAdestrador(Servico servico) {
		adestrador.setId(getIdAdestrador());
		servico.setAdestrador(adestrador);
	}
}
