<h1 class="ls-title-intro">
	<fmt:message key="label.menu.servicos" />
</h1>
<form action="<c:url value='/servico'/>" class="ls-form ls-form-horizontal row " data-ls-module="form" method="post">
	<button type="submit" class="ls-btn-primary btnSalvar">
		<i class="fa fa-floppy-o"></i>
	</button>

	<a href="<c:url value='/listarServicos'/>" class="ls-btn-danger btnCancelar">
		<i class="fa fa-close"></i>
	</a>

<c:if test="${not empty servico.id}">
	<input type="hidden" name="servico.id" value="${servico.id}"/>
	<input type="hidden" name="_method" value="put"/>
</c:if>

<c:if test="${empty servico.id}">
	<input type="hidden" name="_method" value="post"/>
</c:if>
<p>

		<div class="ls-box">

			<div class="row">
				<label class="ls-label col-md-4"> <span
					class="ls-label-text"><fmt:message key="label.descricao" /></span> <input
					type="text" name="servico.descricao" value="${servico.descricao}" required="required">
				</label>
			</div>
			
		</div>
	</form>