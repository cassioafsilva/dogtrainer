<h1 class="ls-title-intro">
	<fmt:message key="label.menu.servicos" />
</h1>
<form action="<c:url value='/servico/${servico.id }'/>" class="ls-form ls-form-horizontal row " data-ls-module="form" method="post">

	<a href="<c:url value="/listarServicos"/>" class="ls-btn btnVoltar">
		<i class="fa fa-reply"></i> 
	</a>
	
	<a href="<c:url value="/servico/editarServico/${servico.id}"/>" class="ls-btn btnEditar">
		<i class="fa fa-edit"></i> 
	</a>
	
	<button type="submit" onclick="return confirm('Deseja realmente remover?');"
			class="ls-btn ls-btn-danger btnExcluir">
		<i class="fa fa-trash"></i>
	</button> 
	<input type="hidden" name="_method" value="DELETE">
	
</form>
<p>	
<div class="ls-box">
 	
 	<label class="ls-label col-md-6 col-lg-8">
      </label>
   	<label class="ls-label col-md-6 col-lg-8">
 		<span>
        <b class="ls-label-text"><fmt:message key="label.descricao"/>:</b>
	      	${servico.descricao}
      	</span>
      </label>
 </div>